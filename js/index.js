let leftDoor = null;
let rightDoor = null;
let tiles = null;
let mainMenuLinks = null;
let subMenuLinks = null;
let tilesContainer = null;
let subMenuContainer = null;

let tile = null;


//doing animation once page is loaded
window.addEventListener("DOMContentLoaded", () => {
    //setting elements
    leftDoor = document.getElementById("left");
    rightDoor = document.getElementById("right");
    tiles = document.getElementsByClassName("tile");
    tilesContainer = document.getElementById("tiles-section");
    subMenuContainer = document.getElementById("second-menu");

    tile = tiles[1].parentNode.outerHTML;

    //play doors animation
    leftDoor.classList.add("move");
    rightDoor.classList.add("move");
    
    //tile animation
    animateTiles("fade-in", 3);
    configureMenus();    
    configureTiles();
});

function animateTiles(anim, timeFactor){
    let tileAnim = "animation: "+anim+" 0.75s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;"

    for(let i = 0; i < tiles.length; i++)
    {
        tiles[i].setAttribute("style", tileAnim + " animation-delay:"+i/timeFactor+"s;");
    }
}

function configureMenus(){
    mainMenuLinks = document.querySelectorAll("#main-menu li");
    subMenuLinks = document.querySelectorAll("#second-menu li");

    for(let j = 0; j < mainMenuLinks.length; j++){
        mainMenuLinks[j].addEventListener("click", () => {
            wipeMainMenuClasses();
            mainMenuLinks[j].classList.add("selected");

            animateTiles("fall-out", 12);
            subMenuContainer.classList.add("fade-out");

            setTimeout(() => {
                changeTiles();
                subMenuContainer.classList.remove("fade-out");
            }, (tiles.length * 1000 + 750 * tiles.length)/12 + 500 ); 
        });
    }

    for(let k = 0; k < subMenuLinks.length; k++){
        subMenuLinks[k].addEventListener("click", () => {
            wipeSubMenuClasses();
            subMenuLinks[k].classList.add("selected");
        });
    }
}

function wipeMainMenuClasses(){
    for(let i = 0; i < mainMenuLinks.length; i++){
        mainMenuLinks[i].classList.remove("selected");
    }
}

function wipeSubMenuClasses(){
    for(let i = 0; i < subMenuLinks.length; i++){
        subMenuLinks[i].classList.remove("selected");
    }
}

function changeTiles(){
    tilesContainer.innerHTML = "";

    //now it's time to import some other tiles, thanks to a little template : tile.html :) 
    insertTile();
}

function recomputeTiles(){
    document.getElementsByClassName("tile");
}

function getRandom(){
    return Math.floor(Math.random() * (8 - 1 + 1)) + 1;
}

function insertTile(){
    let numberOfTiles = getRandom();

    for(let i =0; i < numberOfTiles; i++){
        tilesContainer.innerHTML += tile;
    }

    configureTiles();
    animateTiles("fade-in", 3);
}

function wipeTilesClasses(){
    for (let i = 0; i < tiles.length; i++){
        tiles[i].classList.remove("choosen");
    }
}

function configureTiles(){
    recomputeTiles();
    
    for(let i = 0; i < tiles.length ; i++){
        tiles[i].addEventListener("click", function(){
            wipeTilesClasses();

            if(!tiles[i].classList.contains("choosen"))
                tiles[i].classList.add("choosen");
        });
    }
}

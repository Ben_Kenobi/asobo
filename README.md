# README #

### Install
* Run ```npm install``` or ```yarn install```
* Run ```yarn run serve``` or ```npm run serve```

* Build the app runnin ```yarn run build```

### About

* I configured Parcel JS to help me use a development environment to create this web page (didn't know what would be the different constraints).
* I mainly use SCSS compiled automatically with parcel to be faster on design steps
* Coded using Visual Studio Code

### Infos

* For Asobo Recruitment team.
